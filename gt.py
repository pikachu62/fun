import requests
from bs4 import BeautifulSoup
import time

def main(ar, params):
    url = ar
    client = requests.Session()
    
    if 'tinyurl.com' in url:
        ref_url = client.get(url,allow_redirects=False).headers['location']
        url = ref_url

    if 'gtlinks.me' in url:
        DOMAIN = "https://go.gyanitheme.com"
    else:
        DOMAIN = "https://gtlinks.me"

    url = url[:-1] if url[-1] == '/' else url

    code = url.split("/")[-1]
    if 'gtlinks.me' in url:
        final_url = f"{DOMAIN}/{code}?quelle=" 
    else:
        final_url = f"{DOMAIN}/{code}"
        
    
    resp = client.get(final_url,allow_redirects=False)
    soup = BeautifulSoup(resp.content, "html.parser")
    
    inputs = soup.find(id="go-link").find_all(name="input")
    
    data = { input.get('name'): input.get('value') for input in inputs }

    h = { "x-requested-with": "XMLHttpRequest" }
    
    time.sleep(5)
    r = client.post(f"{DOMAIN}/links/go", data=data, headers=h)
    
    return r.json()
